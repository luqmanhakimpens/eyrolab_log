LogBook 

	06/09/2018{
		1. briefing Lora; 
		2. Studying sx1278f LoraModule
		{
				backGround,
				Specs,
				UseCase,
				interfacing to controller
		};
		3. Soldering sx1278f LoraModule
		{
				SPIBus(MISO,MOSI,SCK,SS),
				power(3.3V, Gnd),
				Antenna(scrapp wire)
		};
		4. installaling SloeberIDE(Arduino plugin for eclipse IDE){
				installing board plugins for{
					GenericArduino boards,
					esp8266 based boards,
					esp32 based boards
				};
				testing functionality of the IDE using nodeMCU board{
					blink sketch,
					blinkWithoutDelay sketch
				}					
		};
		5. testing LORA module using nodeMCU{
				library name: lora_shield_arduino
				example name: TxLora
				{
					testing failed due to lack of knowledge of the library,
					need to study deeper
				}
		};
		
	}
	
	07/09/2018{
		1. testing sx1278f loraModule using Arduino Mega{
				library name: lora_shield_arduino
				example name: TxLora{ note here:
					the pin config on this library is designated for Arduino Uno,
					so the pin mapping for chipSelect(SS) pin is wired to SS pin of Arduino Uno's pin,
					which is digitalPin 10, 
					meanwhile the _default Mega SS pin is on digitalPin 53
				}
		2. discovering the problem of nodeMCU failed to work with sx1278f loraModule{
				the problem presist due to misconfiguration of the reset pin of the sx1278f loraModule,
				this happens because library is written for arduino UNO or Mega board,
				so i have to reconfigure the pinout configuration to be able to work with nodeMCU pinout mapping
			}
		3. testing to send and receive string message using sx1278f loraModule with arduino Mega and nodeMCU on the other end{
				library name: lora_shield_arduino
				example name:{ 2 example here
					TxLora(transmitter, nodeMCU),
					RxLora(receiver, arduino Mega)
				}
				the testing went smoothly, and abled to send and receive in designated address or broadcasting and string messages
			}				
		}
	}
	
	10/09/2018{
		1. testing send and receive message using sx1278f loraModule by modifying addreesses of the module
		{	
			exploring the library(lora_shield_arduino) about the adrressing of the sx1278f loraModule.
			the library says that the adreessing has to be somewhat beetween 0x01 ~ 0xff.
			this mean that it has 255 adreessing number.
		}			
		2. testing wether the module can send or receive messages when the addreesses are mismatced
		{
			the receiver module won't receive any messages from the sender if the sender have mismathed adreess to send to the receiver.
			But, the receiver can receive messages that the sender send to broadcast adreess which is adreess 0x00.
			this allow for secure message transactions
		}
	}
	
	12/09/2018{
		1. testing send and receive message ping-pong using sx1278f loraModule 
		{
			module 1 sends a message to module 2, then module 2 replies with a message to module 1.
			message ping pong is not working perfectly, means that sometimes messages are lost. 
		}
		2. testing message ping pong with addition of the third 3rd module(module 3).
		{
			module 1 sends a message to module 2, then module 2 replies with a message to module 1, 
			then module 1 sends a message to module 3, then module 3 replies with a message to module 1, 
			and so on, and so on.....
			the result also seems doesnt look perfectly work, some messages are lost here and there...
			huft.....
		}
		3.  testing the coverage area of the sx1278f loraModule using 2 modules, one as receiver another as transmitter
		{
			the transmitter is programmed to sends a message to receiver at time interval of 1 second.
			the transmitter sits on a room in the office while the receiver is caried out to the street in front of the office.
			the tester(me) carries the receiver along the street and observes the blink of the LED in the module,
			indicating that receiver is still able to receive the message from the transmitter.
			The RESULT is that the receiver still be able to receive the messages although they separated in couples of meter away,
			(about 30 meters away), with lot of obstacles(home and building).
		}		
				
	}